﻿This is a simple module, developed to work along side the "GateOne" Server .

Gate One is a web-based Terminal Emulator and SSH client that brings the power of the command line to the web. It requires no browser plugins and is built on top of a powerful plugin system that allows every aspect of its appearance and functionality to be customized.

You can find out more about GsteOne here "http://liftoffsoftware.com/Products/GateOne".

As of now the module will only utilize the WEB SSH client, with the toolbar disabled.

This ia a GateOne API implementation, so you will need your API settings. Basic mode can be setup by setting the GasteOne Server to "auth:'none'" and disabling it in the js file of this module, by removing the "auth" section.

REQUIREMENTS
 GateOne Server for the module to connect to.
 

Configuration
1. Downlaod the module .
2. Extract it to your "modules" folder.
3. Go to Drupal 7 Module manager section and enable the module.
4. After its "Enabled", go to the Blocks section.
5. Locate the SSH block and configure it.
6. Provide the GateOne Server URL,API-Key, API-Secret and UPN (Later intergartion with drupal user.).
7. Assign it to a region, and apply and save.
8. SSH into any device securely from the WEB.
9. ENJOY !!!!!